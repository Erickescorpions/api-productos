# API Productos 

## Instalacion en WSL sin Docker

Es necesario tener instalado python3
```
sudo apt install python3-dev
sudo apt install python3-pip
```

Es necesario tener instalado virtualenv, para esto: 
```
pip install virtualenv
```

Despues es necesario crear un entorno virtual:
```
virtualenv -p python3 env
```

Despues de este paso se necesita inciar el entorno virtual con el siguiente comando: 
```
source /env/scripts/activate
```

Para instalar las dependencias dentro de este entorno virtual solo es necesario correr el siguiente comando: 
```
pip install -r requirements.txt
```
### Ejecución 

Para ejecutar la aplicación:
```
python app/app.py
```

Para desactivar el entorno virtual:
```
deactivate
```

## Instalacion y ejecucion con docker

Al tener el Dockerfile solo es necesario construir la imagen para poder empezar a usar la aplicacion
```
docker build -t flask-docker .
```

- -t indica el nombre que va a llevar nuestra imagen.
- . es el contexto con el cual le decimos a docker que debe de buscar el Dockerfile en la carpeta actual.

Despues de construir la imagen es necesario crear un contenedor
```
docker run -it -p 4000:4000 flask-docker
```

- -p indica los puertos que van a ser expuestos para poder interactuar con la aplicacion, en este caso, para la aplicaion se expuso el puerto 4000. 

Se pueden agregar mas banderas al proyecto, como crear un volumen que permita seguir desarrollando el proyecto y pasar en tiempo real nuestro codigo al contenedor. 

Para probar la api puede utilizar software como **insomnia** o **postman**.
